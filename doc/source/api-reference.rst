API reference
=============

.. autoclass:: quickbean.AutoInit
.. autoclass:: quickbean.AutoInitFromJson
.. autoclass:: quickbean.AutoBean
.. autoclass:: quickbean.AutoEq
.. autoclass:: quickbean.AutoRepr
.. autoclass:: quickbean.AutoClone
.. autoclass:: quickbean.AutoToDict
.. autoclass:: quickbean.AutoToJson
