Welcome to QuickBean's documentation !
======================================

QuickBean is a library that reduces the boilerplate code required to define beans.

Installation
------------

Here is how to install QuickBean : ::

    pip install quickbean

Starting with QuickBean
-----------------------

Suppose you have defined the following bean :

.. code:: python

    >>> class MyObject(object):
    ...     def __init__(self, my_property, my_other_property):
    ...         self.my_property = my_property
    ...         self.my_other_property = my_other_property

If you would like your bean to have a human-readable representation, you have to override the *__repr__* method :

.. code:: python

    >>> class MyObject(object):
    ...     def __init__(self, my_property, my_other_property):
    ...         self.my_property = my_property
    ...         self.my_other_property = my_other_property
    ...
    ...     def __repr__(self):
    ...         return 'MyObject(my_property=%s, my_other_property=%s)' % (self.my_property, self.my_other_property)

If you would like your bean to be equality comparable, you also have to override the *__eq__* and *__ne__* methods :

.. code:: python

    >>> class MyObject(object):
    ...     def __init__(self, my_property, my_other_property):
    ...         self.my_property = my_property
    ...         self.my_other_property = my_other_property
    ...
    ...     def __repr__(self):
    ...         return 'MyObject(my_property=%s, my_other_property=%s)' % (self.my_property, self.my_other_property)
    ...
    ...     def __eq__(self, other):
    ...         return other.__class__ is MyObject and other.__dict__ == self.__dict__
    ...
    ...     def __ne__(self, other):
    ...         return not self.__eq__(other)

Although there is nothing difficult here, it would be better if this boilerplate code could be automatically generated
for you. This is exactly what QuickBean brings to you :

.. code:: python

    >>> import quickbean
    >>>
    >>> @quickbean.AutoBean
    ... class MyObject(object):
    ...     def __init__(self, my_property, my_other_property):
    ...         self.my_property = my_property
    ...         self.my_other_property = my_other_property

You may even let QuickBean generate the *__init__* method for you :

.. code:: python

    >>> import quickbean
    >>>
    >>> @quickbean.AutoInit('my_property', 'my_other_property')
    ... @quickbean.AutoBean
    ... class MyObject(object):
    ...     pass

Contents
--------

.. toctree:: api-reference
