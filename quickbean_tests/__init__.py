#
#   Copyright 2014 Olivier Kozak
#
#   This file is part of QuickBean.
#
#   QuickBean is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
#   Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option)
#   any later version.
#
#   QuickBean is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
#   details.
#
#   You should have received a copy of the GNU Lesser General Public License along with QuickBean.  If not, see
#   <http://www.gnu.org/licenses/>.
#

import json
import textwrap

from hamcrest import *

import quickbean


# noinspection PyMethodMayBeStatic
class TestAutoInit(object):
    def test_init(self):
        @quickbean.AutoInit('property_', 'other_property')
        class TestObject(object):
            pass

        # noinspection PyArgumentList
        test_object = TestObject(
            property_='value',
            other_property='otherValue',
        )

        assert_that(test_object.property_, is_(equal_to('value')))
        assert_that(test_object.other_property, is_(equal_to('otherValue')))

    def test_init_using_explicit_form(self):
        @quickbean.AutoInit(
            quickbean.Argument('property_'),
            quickbean.Argument('other_property'),
        )
        class TestObject(object):
            pass

        # noinspection PyArgumentList
        test_object = TestObject(
            property_='value',
            other_property='otherValue',
        )

        assert_that(test_object.property_, is_(equal_to('value')))
        assert_that(test_object.other_property, is_(equal_to('otherValue')))

    def test_init_facing_to_used_default_values(self):
        @quickbean.AutoInit('property_', ('other_property', 'defaultValue'))
        class TestObject(object):
            pass

        # noinspection PyArgumentList
        test_object = TestObject(
            property_='value',
        )

        assert_that(test_object.property_, is_(equal_to('value')))
        assert_that(test_object.other_property, is_(equal_to('defaultValue')))

    def test_init_facing_to_used_default_values_as_non_literals(self):
        # noinspection PyCallingNonCallable
        default_value = object()

        @quickbean.AutoInit('property_', ('other_property', default_value))
        class TestObject(object):
            pass

        # noinspection PyArgumentList
        test_object = TestObject(
            property_='value',
        )

        assert_that(test_object.property_, is_(equal_to('value')))
        assert_that(test_object.other_property, is_(same_instance(default_value)))

    def test_init_facing_to_used_default_values_while_using_explicit_form(self):
        @quickbean.AutoInit(
            quickbean.Argument('property_'),
            quickbean.Argument('other_property', default='defaultValue'),
        )
        class TestObject(object):
            pass

        # noinspection PyArgumentList
        test_object = TestObject(
            property_='value',
        )

        assert_that(test_object.property_, is_(equal_to('value')))
        assert_that(test_object.other_property, is_(equal_to('defaultValue')))

    def test_init_facing_to_unused_default_values(self):
        @quickbean.AutoInit('property_', ('other_property', 'defaultValue'))
        class TestObject(object):
            pass

        # noinspection PyArgumentList
        test_object = TestObject(
            property_='value',
            other_property='otherValue',
        )

        assert_that(test_object.property_, is_(equal_to('value')))
        assert_that(test_object.other_property, is_(equal_to('otherValue')))

    def test_init_facing_to_unused_default_values_as_non_literals(self):
        # noinspection PyCallingNonCallable
        default_value = object()

        @quickbean.AutoInit('property_', ('other_property', default_value))
        class TestObject(object):
            pass

        # noinspection PyArgumentList
        test_object = TestObject(
            property_='value',
            other_property='otherValue',
        )

        assert_that(test_object.property_, is_(equal_to('value')))
        assert_that(test_object.other_property, is_(equal_to('otherValue')))

    def test_init_facing_to_unused_default_values_while_using_explicit_form(self):
        @quickbean.AutoInit(
            quickbean.Argument('property_'),
            quickbean.Argument('other_property', default='defaultValue'),
        )
        class TestObject(object):
            pass

        # noinspection PyArgumentList
        test_object = TestObject(
            property_='value',
            other_property='otherValue',
        )

        assert_that(test_object.property_, is_(equal_to('value')))
        assert_that(test_object.other_property, is_(equal_to('otherValue')))


class TestAutoInitFromJson(object):
    def test_init_from_json_dict(self):
        @quickbean.AutoInitFromJson
        class TestObject(object):
            def __init__(self, property_, other_property):
                self.property_ = property_
                self.other_property = other_property

        test_object = TestObject.from_json_dict({'otherProperty': 'otherValue', 'property_': 'value'})

        assert_that(test_object.property_, is_(equal_to('value')))
        assert_that(test_object.other_property, is_(equal_to('otherValue')))

    def test_init_from_json_dict_with_properties_values_decoded_by_from_json_dict_methods_of_custom_types(self):
        class CustomType(object):
            # noinspection PyMethodMayBeStatic
            def from_json_dict(self, value):
                return '%sFromJson' % value

        @quickbean.AutoInitFromJson
        class TestObject(object):
            def __init__(self, property_, other_property):
                self.property_ = property_
                self.other_property = other_property

            other_property_type = CustomType()

        test_object = TestObject.from_json_dict({'otherProperty': 'otherValue', 'property_': 'value'})

        assert_that(test_object.property_, is_(equal_to('value')))
        assert_that(test_object.other_property, is_(equal_to('otherValueFromJson')))

    def test_init_from_json_str(self):
        @quickbean.AutoInitFromJson
        class TestObject(object):
            def __init__(self, property_, other_property):
                self.property_ = property_
                self.other_property = other_property

        test_object = TestObject.from_json_str('{"otherProperty": "otherValue", "property_": "value"}')

        assert_that(test_object.property_, is_(equal_to('value')))
        assert_that(test_object.other_property, is_(equal_to('otherValue')))

    def test_init_from_json_str_with_properties_values_decoded_by_from_json_str_methods_of_custom_types(self):
        class CustomType(object):
            # noinspection PyMethodMayBeStatic
            def from_json_str(self, value):
                return '%sFromJson' % json.loads(value)

        @quickbean.AutoInitFromJson
        class TestObject(object):
            def __init__(self, property_, other_property):
                self.property_ = property_
                self.other_property = other_property

            other_property_type = CustomType()

        test_object = TestObject.from_json_str('{"otherProperty": "otherValue", "property_": "value"}')

        assert_that(test_object.property_, is_(equal_to('value')))
        assert_that(test_object.other_property, is_(equal_to('otherValueFromJson')))

    def test_init_list_from_json_dict(self):
        @quickbean.AutoInitFromJson
        class TestObject(object):
            def __init__(self, property_):
                self.property_ = property_

        test_objects = TestObject.list_from_json_dict([{'property_': 'value'}, {'property_': 'otherValue'}])

        assert_that(test_objects[0].property_, is_(equal_to('value')))
        assert_that(test_objects[1].property_, is_(equal_to('otherValue')))

    def test_init_list_from_json_str(self):
        @quickbean.AutoInitFromJson
        class TestObject(object):
            def __init__(self, property_):
                self.property_ = property_

        test_objects = TestObject.list_from_json_str('[{"property_": "value"}, {"property_": "otherValue"}]')

        assert_that(test_objects[0].property_, is_(equal_to('value')))
        assert_that(test_objects[1].property_, is_(equal_to('otherValue')))


class TestAutoBean(object):
    def test_eq_against_test_object_with_identical_properties_values(self):
        @quickbean.AutoBean
        class TestObject(object):
            def __init__(self, property_, other_property):
                self.property_ = property_
                self.other_property = other_property

        test_object = TestObject(
            property_='value',
            other_property='otherValue',
        )

        test_object_with_identical_properties_values = TestObject(
            property_='value',
            other_property='otherValue',
        )

        assert_that(test_object == test_object_with_identical_properties_values)
        assert_that(not test_object != test_object_with_identical_properties_values)

    def test_eq_against_test_object_with_identical_properties_but_different_extra_properties_values(self):
        @quickbean.AutoBean
        class TestObject(object):
            def __init__(self, property_, other_property, extra_property):
                self.property_ = property_
                self.other_property = other_property
                self.extra_property = extra_property

            all_properties = ['property_', 'other_property']

        test_object = TestObject(
            property_='value',
            other_property='otherValue',
            extra_property='extraValue',
        )

        test_object_with_identical_properties_but_different_extra_properties_values = TestObject(
            property_='value',
            other_property='otherValue',
            extra_property='differentExtraValue',
        )

        assert_that(test_object == test_object_with_identical_properties_but_different_extra_properties_values)
        assert_that(not test_object != test_object_with_identical_properties_but_different_extra_properties_values)

    def test_eq_against_test_object_with_identical_properties_but_different_hidden_properties_values(self):
        @quickbean.AutoBean
        class TestObject(object):
            def __init__(self, property_, other_property, _hidden_property):
                self.property_ = property_
                self.other_property = other_property
                self._hidden_property = _hidden_property

        test_object = TestObject(
            property_='value',
            other_property='otherValue',
            _hidden_property='hiddenValue',
        )

        test_object_with_identical_properties_but_different_hidden_properties_values = TestObject(
            property_='value',
            other_property='otherValue',
            _hidden_property='differentHiddenValue',
        )

        assert_that(test_object == test_object_with_identical_properties_but_different_hidden_properties_values)
        assert_that(not test_object != test_object_with_identical_properties_but_different_hidden_properties_values)

    def test_eq_against_test_object_with_identical_properties_but_different_excluded_properties_values(self):
        @quickbean.AutoBean(quickbean.exclude_properties('excluded_property'))
        class TestObject(object):
            def __init__(self, property_, other_property, excluded_property):
                self.property_ = property_
                self.other_property = other_property
                self.excluded_property = excluded_property

        test_object = TestObject(
            property_='value',
            other_property='otherValue',
            excluded_property='excludedValue',
        )

        test_object_with_identical_properties_but_different_excluded_properties_values = TestObject(
            property_='value',
            other_property='otherValue',
            excluded_property='differentExcludedValue',
        )

        assert_that(test_object == test_object_with_identical_properties_but_different_excluded_properties_values)
        assert_that(not test_object != test_object_with_identical_properties_but_different_excluded_properties_values)

    def test_repr(self):
        @quickbean.AutoBean
        class TestObject(object):
            def __init__(self, property_, other_property):
                self.property_ = property_
                self.other_property = other_property

        test_object = TestObject(
            property_='value',
            other_property='otherValue',
        )

        assert_that(repr(test_object), is_(equal_to(
            "TestObject(other_property='otherValue', property_='value')"
        )))

    def test_repr_with_extra_properties(self):
        @quickbean.AutoBean
        class TestObject(object):
            def __init__(self, property_, other_property, extra_property):
                self.property_ = property_
                self.other_property = other_property
                self.extra_property = extra_property

            all_properties = ['property_', 'other_property']

        test_object = TestObject(
            property_='value',
            other_property='otherValue',
            extra_property='extraValue',
        )

        assert_that(repr(test_object), is_(equal_to(
            "TestObject(other_property='otherValue', property_='value')"
        )))

    def test_repr_with_hidden_properties(self):
        @quickbean.AutoBean
        class TestObject(object):
            def __init__(self, property_, other_property, _hidden_property):
                self.property_ = property_
                self.other_property = other_property
                self._hidden_property = _hidden_property

        test_object = TestObject(
            property_='value',
            other_property='otherValue',
            _hidden_property='hiddenValue',
        )

        assert_that(repr(test_object), is_(equal_to(
            "TestObject(other_property='otherValue', property_='value')"
        )))

    def test_repr_with_excluded_properties(self):
        @quickbean.AutoBean(quickbean.exclude_properties('excluded_property'))
        class TestObject(object):
            def __init__(self, property_, other_property, excluded_property):
                self.property_ = property_
                self.other_property = other_property
                self.excluded_property = excluded_property

        test_object = TestObject(
            property_='value',
            other_property='otherValue',
            excluded_property='excludedValue',
        )

        assert_that(repr(test_object), is_(equal_to(
            "TestObject(other_property='otherValue', property_='value')"
        )))

    def test_clone(self):
        @quickbean.AutoBean
        class TestObject(object):
            def __init__(self, property_, other_property):
                self.property_ = property_
                self.other_property = other_property

        test_object = TestObject(
            property_='value',
            other_property='otherValue',
        )

        test_object_clone = test_object.clone()

        assert_that(test_object_clone, is_(instance_of(TestObject)))
        assert_that(test_object_clone, is_not(same_instance(test_object)))

        assert_that(test_object_clone.property_, is_(equal_to('value')))
        assert_that(test_object_clone.other_property, is_(equal_to('otherValue')))

    def test_clone_with_extra_properties(self):
        @quickbean.AutoBean
        class TestObject(object):
            def __init__(self, property_, other_property, extra_property=None):
                self.property_ = property_
                self.other_property = other_property
                self.extra_property = extra_property

            all_properties = ['property_', 'other_property']

        test_object = TestObject(
            property_='value',
            other_property='otherValue',
            extra_property='extraValue',
        )

        test_object_clone = test_object.clone()

        assert_that(test_object_clone, is_(instance_of(TestObject)))
        assert_that(test_object_clone, is_not(same_instance(test_object)))

        assert_that(test_object_clone.property_, is_(equal_to('value')))
        assert_that(test_object_clone.other_property, is_(equal_to('otherValue')))
        assert_that(test_object_clone.extra_property, is_(none()))

    def test_clone_with_hidden_properties(self):
        @quickbean.AutoBean
        class TestObject(object):
            def __init__(self, property_, other_property, _hidden_property=None):
                self.property_ = property_
                self.other_property = other_property
                self._hidden_property = _hidden_property

        test_object = TestObject(
            property_='value',
            other_property='otherValue',
            _hidden_property='hiddenValue',
        )

        test_object_clone = test_object.clone()

        assert_that(test_object_clone, is_(instance_of(TestObject)))
        assert_that(test_object_clone, is_not(same_instance(test_object)))

        assert_that(test_object_clone.property_, is_(equal_to('value')))
        assert_that(test_object_clone.other_property, is_(equal_to('otherValue')))
        assert_that(test_object_clone._hidden_property, is_(none()))

    def test_clone_with_excluded_properties(self):
        @quickbean.AutoBean(quickbean.exclude_properties('excluded_property'))
        class TestObject(object):
            def __init__(self, property_, other_property, excluded_property=None):
                self.property_ = property_
                self.other_property = other_property
                self.excluded_property = excluded_property

        test_object = TestObject(
            property_='value',
            other_property='otherValue',
            excluded_property='excludedValue',
        )

        test_object_clone = test_object.clone()

        assert_that(test_object_clone, is_(instance_of(TestObject)))
        assert_that(test_object_clone, is_not(same_instance(test_object)))

        assert_that(test_object_clone.property_, is_(equal_to('value')))
        assert_that(test_object_clone.other_property, is_(equal_to('otherValue')))
        assert_that(test_object_clone.excluded_property, is_(none()))

    def test_to_dict(self):
        @quickbean.AutoBean
        class TestObject(object):
            def __init__(self, property_, other_property):
                self.property_ = property_
                self.other_property = other_property

        test_object = TestObject(
            property_='value',
            other_property='otherValue',
        )

        assert_that(test_object.to_dict(), is_(equal_to({
            'property_': 'value',
            'other_property': 'otherValue',
        })))

    def test_to_dict_with_extra_properties(self):
        @quickbean.AutoBean
        class TestObject(object):
            def __init__(self, property_, other_property, extra_property):
                self.property_ = property_
                self.other_property = other_property
                self.extra_property = extra_property

            all_properties = ['property_', 'other_property']

        test_object = TestObject(
            property_='value',
            other_property='otherValue',
            extra_property='extraValue',
        )

        assert_that(test_object.to_dict(), is_(equal_to({
            'property_': 'value',
            'other_property': 'otherValue',
        })))

    def test_to_dict_with_hidden_properties(self):
        @quickbean.AutoBean
        class TestObject(object):
            def __init__(self, property_, other_property, _hidden_property):
                self.property_ = property_
                self.other_property = other_property
                self._hidden_property = _hidden_property

        test_object = TestObject(
            property_='value',
            other_property='otherValue',
            _hidden_property='hiddenValue',
        )

        assert_that(test_object.to_dict(), is_(equal_to({
            'property_': 'value',
            'other_property': 'otherValue',
        })))

    def test_to_dict_with_excluded_properties(self):
        @quickbean.AutoBean(quickbean.exclude_properties('excluded_property'))
        class TestObject(object):
            def __init__(self, property_, other_property, excluded_property):
                self.property_ = property_
                self.other_property = other_property
                self.excluded_property = excluded_property

        test_object = TestObject(
            property_='value',
            other_property='otherValue',
            excluded_property='excludedValue',
        )

        assert_that(test_object.to_dict(), is_(equal_to({
            'property_': 'value',
            'other_property': 'otherValue',
        })))

    def test_to_json_str(self):
        @quickbean.AutoBean
        class TestObject(object):
            def __init__(self, property_, other_property):
                self.property_ = property_
                self.other_property = other_property

        test_object = TestObject(
            property_='value',
            other_property='otherValue',
        )

        assert_that(test_object.to_json_str(), is_(equal_to(
            '{"otherProperty": "otherValue", "property_": "value"}'
        )))

    def test_to_json_str_with_extra_properties(self):
        @quickbean.AutoBean
        class TestObject(object):
            def __init__(self, property_, other_property, extra_property):
                self.property_ = property_
                self.other_property = other_property
                self.extra_property = extra_property

            all_properties = ['property_', 'other_property']

        test_object = TestObject(
            property_='value',
            other_property='otherValue',
            extra_property='extraValue',
        )

        assert_that(test_object.to_json_str(), is_(equal_to(
            '{"otherProperty": "otherValue", "property_": "value"}'
        )))

    def test_to_json_str_with_hidden_properties(self):
        @quickbean.AutoBean
        class TestObject(object):
            def __init__(self, property_, other_property, _hidden_property):
                self.property_ = property_
                self.other_property = other_property
                self._hidden_property = _hidden_property

        test_object = TestObject(
            property_='value',
            other_property='otherValue',
            _hidden_property='hiddenValue',
        )

        assert_that(test_object.to_json_str(), is_(equal_to(
            '{"otherProperty": "otherValue", "property_": "value"}'
        )))

    def test_to_json_str_with_excluded_properties(self):
        @quickbean.AutoBean(quickbean.exclude_properties('excluded_property'))
        class TestObject(object):
            def __init__(self, property_, other_property, excluded_property):
                self.property_ = property_
                self.other_property = other_property
                self.excluded_property = excluded_property

        test_object = TestObject(
            property_='value',
            other_property='otherValue',
            excluded_property='excludedValue',
        )

        assert_that(test_object.to_json_str(), is_(equal_to(
            '{"otherProperty": "otherValue", "property_": "value"}'
        )))


class TestAutoEq(object):
    def test_eq_against_none(self):
        @quickbean.AutoEq
        class TestObject(object):
            def __init__(self, property_, other_property):
                self.property_ = property_
                self.other_property = other_property

        test_object = TestObject(
            property_='value',
            other_property='otherValue',
        )

        none_test_object = None

        assert_that(test_object != none_test_object)
        assert_that(not test_object == none_test_object)

    def test_eq_against_test_object_with_identical_properties_values(self):
        @quickbean.AutoEq
        class TestObject(object):
            def __init__(self, property_, other_property):
                self.property_ = property_
                self.other_property = other_property

        test_object = TestObject(
            property_='value',
            other_property='otherValue',
        )

        test_object_with_identical_properties_values = TestObject(
            property_='value',
            other_property='otherValue',
        )

        assert_that(test_object == test_object_with_identical_properties_values)
        assert_that(not test_object != test_object_with_identical_properties_values)

    def test_eq_against_test_object_with_identical_properties_but_different_extra_properties_values(self):
        @quickbean.AutoEq
        class TestObject(object):
            def __init__(self, property_, other_property, extra_property):
                self.property_ = property_
                self.other_property = other_property
                self.extra_property = extra_property

            all_properties = ['property_', 'other_property']

        test_object = TestObject(
            property_='value',
            other_property='otherValue',
            extra_property='extraValue',
        )

        test_object_with_identical_properties_but_different_extra_properties_values = TestObject(
            property_='value',
            other_property='otherValue',
            extra_property='differentExtraValue',
        )

        assert_that(test_object == test_object_with_identical_properties_but_different_extra_properties_values)
        assert_that(not test_object != test_object_with_identical_properties_but_different_extra_properties_values)

    def test_eq_against_test_object_with_identical_properties_but_different_hidden_properties_values(self):
        @quickbean.AutoEq
        class TestObject(object):
            def __init__(self, property_, other_property, _hidden_property):
                self.property_ = property_
                self.other_property = other_property
                self._hidden_property = _hidden_property

        test_object = TestObject(
            property_='value',
            other_property='otherValue',
            _hidden_property='hiddenValue',
        )

        test_object_with_identical_properties_but_different_hidden_properties_values = TestObject(
            property_='value',
            other_property='otherValue',
            _hidden_property='differentHiddenValue',
        )

        assert_that(test_object == test_object_with_identical_properties_but_different_hidden_properties_values)
        assert_that(not test_object != test_object_with_identical_properties_but_different_hidden_properties_values)

    def test_eq_against_test_object_with_identical_properties_but_different_excluded_properties_values(self):
        @quickbean.AutoEq(quickbean.exclude_properties('excluded_property'))
        class TestObject(object):
            def __init__(self, property_, other_property, excluded_property):
                self.property_ = property_
                self.other_property = other_property
                self.excluded_property = excluded_property

        test_object = TestObject(
            property_='value',
            other_property='otherValue',
            excluded_property='excludedValue',
        )

        test_object_with_identical_properties_but_different_excluded_properties_values = TestObject(
            property_='value',
            other_property='otherValue',
            excluded_property='differentExcludedValue',
        )

        assert_that(test_object == test_object_with_identical_properties_but_different_excluded_properties_values)
        assert_that(not test_object != test_object_with_identical_properties_but_different_excluded_properties_values)

    # noinspection PyPep8
    def test_eq_against_test_object_with_identical_properties_but_properties_equalities_given_by_eq_methods_mocking_to_identical_overridden_properties_values(self):
        @quickbean.AutoEq
        class TestObject(object):
            def __init__(self, property_, other_property, overridden_property):
                self.property_ = property_
                self.other_property = other_property
                self.overridden_property = overridden_property

            def overridden_property_eq(self, other_value):
                return other_value != self.overridden_property

        test_object = TestObject(
            property_='value',
            other_property='otherValue',
            overridden_property='overriddenValue',
        )

        test_object_with_identical_properties_but_properties_equalities_given_by_eq_methods_mocking_to_identical_overridden_properties_values = TestObject(
            property_='value',
            other_property='otherValue',
            overridden_property='differentOverriddenValue',
        )

        assert_that(test_object == test_object_with_identical_properties_but_properties_equalities_given_by_eq_methods_mocking_to_identical_overridden_properties_values)
        assert_that(not test_object != test_object_with_identical_properties_but_properties_equalities_given_by_eq_methods_mocking_to_identical_overridden_properties_values)

    # noinspection PyPep8
    def test_eq_against_test_object_with_identical_properties_but_properties_equalities_given_by_eq_methods_mocking_to_different_overridden_properties_values(self):
        @quickbean.AutoEq
        class TestObject(object):
            def __init__(self, property_, other_property, overridden_property):
                self.property_ = property_
                self.other_property = other_property
                self.overridden_property = overridden_property

            def overridden_property_eq(self, other_value):
                return other_value != self.overridden_property

        test_object = TestObject(
            property_='value',
            other_property='otherValue',
            overridden_property='overriddenValue',
        )

        test_object_with_identical_properties_but_properties_equalities_given_by_eq_methods_mocking_to_different_overridden_properties_values = TestObject(
            property_='value',
            other_property='otherValue',
            overridden_property='overriddenValue',
        )

        assert_that(test_object != test_object_with_identical_properties_but_properties_equalities_given_by_eq_methods_mocking_to_different_overridden_properties_values)
        assert_that(not test_object == test_object_with_identical_properties_but_properties_equalities_given_by_eq_methods_mocking_to_different_overridden_properties_values)

    # noinspection PyPep8
    def test_eq_against_test_object_with_identical_properties_but_properties_equalities_given_by_eq_methods_of_custom_types_mocking_to_identical_overridden_properties_values(self):
        class CustomType(object):
            # noinspection PyMethodMayBeStatic
            def eq_of(self, value, other_value):
                return value != other_value

        @quickbean.AutoEq
        class TestObject(object):
            def __init__(self, property_, other_property, overridden_property):
                self.property_ = property_
                self.other_property = other_property
                self.overridden_property = overridden_property

            overridden_property_type = CustomType()

        test_object = TestObject(
            property_='value',
            other_property='otherValue',
            overridden_property='overriddenValue',
        )

        test_object_with_identical_properties_but_properties_equalities_given_by_eq_methods_of_custom_types_mocking_to_identical_overridden_properties_values = TestObject(
            property_='value',
            other_property='otherValue',
            overridden_property='differentOverriddenValue',
        )

        assert_that(test_object == test_object_with_identical_properties_but_properties_equalities_given_by_eq_methods_of_custom_types_mocking_to_identical_overridden_properties_values)
        assert_that(not test_object != test_object_with_identical_properties_but_properties_equalities_given_by_eq_methods_of_custom_types_mocking_to_identical_overridden_properties_values)

    # noinspection PyPep8
    def test_eq_against_test_object_with_identical_properties_but_properties_equalities_given_by_eq_methods_of_custom_types_mocking_to_different_overridden_properties_values(self):
        class CustomType(object):
            # noinspection PyMethodMayBeStatic
            def eq_of(self, value, other_value):
                return value != other_value

        @quickbean.AutoEq
        class TestObject(object):
            def __init__(self, property_, other_property, overridden_property):
                self.property_ = property_
                self.other_property = other_property
                self.overridden_property = overridden_property

            overridden_property_type = CustomType()

        test_object = TestObject(
            property_='value',
            other_property='otherValue',
            overridden_property='overriddenValue',
        )

        test_object_with_identical_properties_but_properties_equalities_given_by_eq_methods_of_custom_types_mocking_to_different_overridden_properties_values = TestObject(
            property_='value',
            other_property='otherValue',
            overridden_property='overriddenValue',
        )

        assert_that(test_object != test_object_with_identical_properties_but_properties_equalities_given_by_eq_methods_of_custom_types_mocking_to_different_overridden_properties_values)
        assert_that(not test_object == test_object_with_identical_properties_but_properties_equalities_given_by_eq_methods_of_custom_types_mocking_to_different_overridden_properties_values)

    def test_eq_against_test_object_with_different_properties_values(self):
        @quickbean.AutoEq
        class TestObject(object):
            def __init__(self, property_, other_property):
                self.property_ = property_
                self.other_property = other_property

        test_object = TestObject(
            property_='value',
            other_property='otherValue',
        )

        test_object_with_different_properties_values = TestObject(
            property_='value',
            other_property='differentOtherValue',
        )

        assert_that(test_object != test_object_with_different_properties_values)
        assert_that(not test_object == test_object_with_different_properties_values)

    def test_eq_against_other_test_object_with_identical_properties_values(self):
        @quickbean.AutoEq
        class TestObject(object):
            def __init__(self, property_, other_property):
                self.property_ = property_
                self.other_property = other_property

        @quickbean.AutoEq
        class OtherTestObject(object):
            def __init__(self, property_, other_property):
                self.property_ = property_
                self.other_property = other_property

        test_object = TestObject(
            property_='value',
            other_property='otherValue',
        )

        other_test_object_with_identical_properties_values = OtherTestObject(
            property_='value',
            other_property='otherValue',
        )

        assert_that(test_object != other_test_object_with_identical_properties_values)
        assert_that(not test_object == other_test_object_with_identical_properties_values)

    def test_eq_against_test_object_with_identical_descriptors_values(self):
        @quickbean.AutoEq
        class TestObject(object):
            def __init__(self, property_, other_property):
                self._properties = {
                    'property_': property_,
                    'other_property': other_property,
                }

            @property
            def property_(self):
                return self._properties['property_']

            @property
            def other_property(self):
                return self._properties['other_property']

        test_object = TestObject(
            property_='value',
            other_property='otherValue',
        )

        test_object_with_identical_properties_values = TestObject(
            property_='value',
            other_property='otherValue',
        )

        assert_that(test_object == test_object_with_identical_properties_values)
        assert_that(not test_object != test_object_with_identical_properties_values)

    def test_eq_against_test_object_with_different_descriptors_values(self):
        @quickbean.AutoEq
        class TestObject(object):
            def __init__(self, property_, other_property):
                self._properties = {
                    'property_': property_,
                    'other_property': other_property,
                }

            @property
            def property_(self):
                return self._properties['property_']

            @property
            def other_property(self):
                return self._properties['other_property']

        test_object = TestObject(
            property_='value',
            other_property='otherValue',
        )

        test_object_with_different_properties_values = TestObject(
            property_='value',
            other_property='differentOtherValue',
        )

        assert_that(test_object != test_object_with_different_properties_values)
        assert_that(not test_object == test_object_with_different_properties_values)

    def test_eq_against_other_test_object_with_identical_descriptors_values(self):
        @quickbean.AutoEq
        class TestObject(object):
            def __init__(self, property_, other_property):
                self._properties = {
                    'property_': property_,
                    'other_property': other_property,
                }

            @property
            def property_(self):
                return self._properties['property_']

            @property
            def other_property(self):
                return self._properties['other_property']

        @quickbean.AutoEq
        class OtherTestObject(object):
            def __init__(self, property_, other_property):
                self._properties = {
                    'property_': property_,
                    'other_property': other_property,
                }

            @property
            def property_(self):
                return self._properties['property_']

            @property
            def other_property(self):
                return self._properties['other_property']

        test_object = TestObject(
            property_='value',
            other_property='otherValue',
        )

        other_test_object_with_identical_properties_values = OtherTestObject(
            property_='value',
            other_property='otherValue',
        )

        assert_that(test_object != other_test_object_with_identical_properties_values)
        assert_that(not test_object == other_test_object_with_identical_properties_values)


class TestAutoRepr(object):
    def test_repr(self):
        @quickbean.AutoRepr
        class TestObject(object):
            def __init__(self, property_, other_property):
                self.property_ = property_
                self.other_property = other_property

        test_object = TestObject(
            property_='value',
            other_property='otherValue',
        )

        assert_that(repr(test_object), is_(equal_to(
            "TestObject(other_property='otherValue', property_='value')"
        )))

    def test_repr_with_extra_properties(self):
        @quickbean.AutoRepr
        class TestObject(object):
            def __init__(self, property_, other_property, extra_property):
                self.property_ = property_
                self.other_property = other_property
                self.extra_property = extra_property

            all_properties = ['property_', 'other_property']

        test_object = TestObject(
            property_='value',
            other_property='otherValue',
            extra_property='extraValue',
        )

        assert_that(repr(test_object), is_(equal_to(
            "TestObject(other_property='otherValue', property_='value')"
        )))

    def test_repr_with_hidden_properties(self):
        @quickbean.AutoRepr
        class TestObject(object):
            def __init__(self, property_, other_property, _hidden_property):
                self.property_ = property_
                self.other_property = other_property
                self._hidden_property = _hidden_property

        test_object = TestObject(
            property_='value',
            other_property='otherValue',
            _hidden_property='hiddenValue',
        )

        assert_that(repr(test_object), is_(equal_to(
            "TestObject(other_property='otherValue', property_='value')"
        )))

    def test_repr_with_excluded_properties(self):
        @quickbean.AutoRepr(quickbean.exclude_properties('excluded_property'))
        class TestObject(object):
            def __init__(self, property_, other_property, excluded_property):
                self.property_ = property_
                self.other_property = other_property
                self.excluded_property = excluded_property

        test_object = TestObject(
            property_='value',
            other_property='otherValue',
            excluded_property='excludedValue',
        )

        assert_that(repr(test_object), is_(equal_to(
            "TestObject(other_property='otherValue', property_='value')"
        )))

    def test_repr_with_properties_representations_given_by_repr_methods(self):
        @quickbean.AutoRepr
        class TestObject(object):
            def __init__(self, property_, other_property):
                self.property_ = property_
                self.other_property = other_property

            # noinspection PyMethodMayBeStatic
            def other_property_repr(self):
                return "'%sRepr'" % self.other_property

        test_object = TestObject(
            property_='value',
            other_property='otherValue',
        )

        assert_that(repr(test_object), is_(equal_to(
            "TestObject(other_property='otherValueRepr', property_='value')"
        )))

    def test_repr_with_properties_representations_given_by_repr_methods_of_custom_types(self):
        class CustomType(object):
            # noinspection PyMethodMayBeStatic
            def repr_of(self, value):
                return "'%sRepr'" % value

        @quickbean.AutoRepr
        class TestObject(object):
            def __init__(self, property_, other_property):
                self.property_ = property_
                self.other_property = other_property

            other_property_type = CustomType()

        test_object = TestObject(
            property_='value',
            other_property='otherValue',
        )

        assert_that(repr(test_object), is_(equal_to(
            "TestObject(other_property='otherValueRepr', property_='value')"
        )))

    def test_repr_with_descriptors(self):
        @quickbean.AutoRepr
        class TestObject(object):
            def __init__(self, property_, other_property):
                self._properties = {
                    'property_': property_,
                    'other_property': other_property,
                }

            @property
            def property_(self):
                return self._properties['property_']

            @property
            def other_property(self):
                return self._properties['other_property']

        test_object = TestObject(
            property_='value',
            other_property='otherValue',
        )

        assert_that(repr(test_object), is_(equal_to(
            "TestObject(other_property='otherValue', property_='value')"
        )))


class TestAutoClone(object):
    def test_clone(self):
        @quickbean.AutoClone
        class TestObject(object):
            def __init__(self, property_, other_property):
                self.property_ = property_
                self.other_property = other_property

        test_object = TestObject(
            property_='value',
            other_property='otherValue',
        )

        test_object_clone = test_object.clone()

        assert_that(test_object_clone, is_(instance_of(TestObject)))
        assert_that(test_object_clone, is_not(same_instance(test_object)))

        assert_that(test_object_clone.property_, is_(equal_to('value')))
        assert_that(test_object_clone.other_property, is_(equal_to('otherValue')))

    def test_clone_with_extra_properties(self):
        @quickbean.AutoClone
        class TestObject(object):
            def __init__(self, property_, other_property, extra_property=None):
                self.property_ = property_
                self.other_property = other_property
                self.extra_property = extra_property

            all_properties = ['property_', 'other_property']

        test_object = TestObject(
            property_='value',
            other_property='otherValue',
            extra_property='extraValue',
        )

        test_object_clone = test_object.clone()

        assert_that(test_object_clone, is_(instance_of(TestObject)))
        assert_that(test_object_clone, is_not(same_instance(test_object)))

        assert_that(test_object_clone.property_, is_(equal_to('value')))
        assert_that(test_object_clone.other_property, is_(equal_to('otherValue')))
        assert_that(test_object_clone.extra_property, is_(none()))

    def test_clone_with_hidden_properties(self):
        @quickbean.AutoClone
        class TestObject(object):
            def __init__(self, property_, other_property, _hidden_property=None):
                self.property_ = property_
                self.other_property = other_property
                self._hidden_property = _hidden_property

        test_object = TestObject(
            property_='value',
            other_property='otherValue',
            _hidden_property='hiddenValue',
        )

        test_object_clone = test_object.clone()

        assert_that(test_object_clone, is_(instance_of(TestObject)))
        assert_that(test_object_clone, is_not(same_instance(test_object)))

        assert_that(test_object_clone.property_, is_(equal_to('value')))
        assert_that(test_object_clone.other_property, is_(equal_to('otherValue')))
        assert_that(test_object_clone._hidden_property, is_(none()))

    def test_clone_with_excluded_properties(self):
        @quickbean.AutoClone(quickbean.exclude_properties('excluded_property'))
        class TestObject(object):
            def __init__(self, property_, other_property, excluded_property=None):
                self.property_ = property_
                self.other_property = other_property
                self.excluded_property = excluded_property

        test_object = TestObject(
            property_='value',
            other_property='otherValue',
            excluded_property='excludedValue',
        )

        test_object_clone = test_object.clone()

        assert_that(test_object_clone, is_(instance_of(TestObject)))
        assert_that(test_object_clone, is_not(same_instance(test_object)))

        assert_that(test_object_clone.property_, is_(equal_to('value')))
        assert_that(test_object_clone.other_property, is_(equal_to('otherValue')))
        assert_that(test_object_clone.excluded_property, is_(none()))

    def test_clone_with_overridden_properties(self):
        @quickbean.AutoClone
        class TestObject(object):
            def __init__(self, property_, other_property):
                self.property_ = property_
                self.other_property = other_property

        test_object = TestObject(
            property_='value',
            other_property='otherValue',
        )

        test_object_clone = test_object.clone(other_property='overriddenOtherValue')

        assert_that(test_object_clone, is_(instance_of(TestObject)))
        assert_that(test_object_clone, is_not(same_instance(test_object)))

        assert_that(test_object_clone.property_, is_(equal_to('value')))
        assert_that(test_object_clone.other_property, is_(equal_to('overriddenOtherValue')))

    def test_clone_with_descriptors(self):
        @quickbean.AutoClone
        class TestObject(object):
            def __init__(self, property_, other_property):
                self._properties = {
                    'property_': property_,
                    'other_property': other_property,
                }

            @property
            def property_(self):
                return self._properties['property_']

            @property
            def other_property(self):
                return self._properties['other_property']

        test_object = TestObject(
            property_='value',
            other_property='otherValue',
        )

        test_object_clone = test_object.clone()

        assert_that(test_object_clone, is_(instance_of(TestObject)))
        assert_that(test_object_clone, is_not(same_instance(test_object)))

        assert_that(test_object_clone.property_, is_(equal_to('value')))
        assert_that(test_object_clone.other_property, is_(equal_to('otherValue')))


class TestAutoToDict(object):
    def test_to_dict(self):
        @quickbean.AutoToDict
        class TestObject(object):
            def __init__(self, property_, other_property):
                self.property_ = property_
                self.other_property = other_property

        test_object = TestObject(
            property_='value',
            other_property='otherValue',
        )

        assert_that(test_object.to_dict(), is_(equal_to({
            'property_': 'value',
            'other_property': 'otherValue',
        })))

    def test_to_dict_with_extra_properties(self):
        @quickbean.AutoToDict
        class TestObject(object):
            def __init__(self, property_, other_property, extra_property):
                self.property_ = property_
                self.other_property = other_property
                self.extra_property = extra_property

            all_properties = ['property_', 'other_property']

        test_object = TestObject(
            property_='value',
            other_property='otherValue',
            extra_property='extraValue',
        )

        assert_that(test_object.to_dict(), is_(equal_to({
            'property_': 'value',
            'other_property': 'otherValue',
        })))

    def test_to_dict_with_hidden_properties(self):
        @quickbean.AutoToDict
        class TestObject(object):
            def __init__(self, property_, other_property, _hidden_property):
                self.property_ = property_
                self.other_property = other_property
                self._hidden_property = _hidden_property

        test_object = TestObject(
            property_='value',
            other_property='otherValue',
            _hidden_property='hiddenValue',
        )

        assert_that(test_object.to_dict(), is_(equal_to({
            'property_': 'value',
            'other_property': 'otherValue',
        })))

    def test_to_dict_with_excluded_properties(self):
        @quickbean.AutoToDict(quickbean.exclude_properties('excluded_property'))
        class TestObject(object):
            def __init__(self, property_, other_property, excluded_property):
                self.property_ = property_
                self.other_property = other_property
                self.excluded_property = excluded_property

        test_object = TestObject(
            property_='value',
            other_property='otherValue',
            excluded_property='excludedValue',
        )

        assert_that(test_object.to_dict(), is_(equal_to({
            'property_': 'value',
            'other_property': 'otherValue',
        })))

    def test_to_dict_with_descriptors(self):
        @quickbean.AutoToDict
        class TestObject(object):
            def __init__(self, property_, other_property):
                self._properties = {
                    'property_': property_,
                    'other_property': other_property,
                }

            @property
            def property_(self):
                return self._properties['property_']

            @property
            def other_property(self):
                return self._properties['other_property']

        test_object = TestObject(
            property_='value',
            other_property='otherValue',
        )

        assert_that(test_object.to_dict(), is_(equal_to({
            'property_': 'value',
            'other_property': 'otherValue',
        })))


class TestAutoToJson(object):
    def test_to_json_dict(self):
        @quickbean.AutoToJson
        class TestObject(object):
            def __init__(self, property_, other_property):
                self.property_ = property_
                self.other_property = other_property

        test_object = TestObject(
            property_='value',
            other_property='otherValue',
        )

        assert_that(test_object.to_json_dict(), is_(equal_to(
            {'otherProperty': 'otherValue', 'property_': 'value'}
        )))

    def test_to_json_dict_with_properties_values_encoded_by_to_json_dict(self):
        @quickbean.AutoToJson
        class TestObject(object):
            def __init__(self, property_, other_property):
                self.property_ = property_
                self.other_property = other_property

            # noinspection PyMethodMayBeStatic
            def other_property_to_json_dict(self):
                return '%sToJson' % self.other_property

        test_object = TestObject(
            property_='value',
            other_property='otherValue',
        )

        assert_that(test_object.to_json_dict(), is_(equal_to(
            {'otherProperty': 'otherValueToJson', 'property_': 'value'}
        )))

    def test_to_json_dict_with_properties_values_encoded_by_to_json_dict_methods_of_custom_types(self):
        class CustomType(object):
            # noinspection PyMethodMayBeStatic
            def to_json_dict(self, value):
                return '%sToJson' % value

        @quickbean.AutoToJson
        class TestObject(object):
            def __init__(self, property_, other_property):
                self.property_ = property_
                self.other_property = other_property

            other_property_type = CustomType()

        test_object = TestObject(
            property_='value',
            other_property='otherValue',
        )

        assert_that(test_object.to_json_dict(), is_(equal_to(
            {'otherProperty': 'otherValueToJson', 'property_': 'value'}
        )))

    def test_to_json_dict_with_descriptors(self):
        @quickbean.AutoToJson
        class TestObject(object):
            def __init__(self, property_, other_property):
                self._properties = {
                    'property_': property_,
                    'other_property': other_property,
                }

            @property
            def property_(self):
                return self._properties['property_']

            @property
            def other_property(self):
                return self._properties['other_property']

        test_object = TestObject(
            property_='value',
            other_property='otherValue',
        )

        assert_that(test_object.to_json_dict(), is_(equal_to(
            {'otherProperty': 'otherValue', 'property_': 'value'}
        )))

    def test_to_json_str(self):
        @quickbean.AutoToJson
        class TestObject(object):
            def __init__(self, property_, other_property):
                self.property_ = property_
                self.other_property = other_property

        test_object = TestObject(
            property_='value',
            other_property='otherValue',
        )

        assert_that(test_object.to_json_str(), is_(equal_to(
            '{"otherProperty": "otherValue", "property_": "value"}'
        )))

    def test_to_json_str_with_extra_properties(self):
        @quickbean.AutoToJson
        class TestObject(object):
            def __init__(self, property_, other_property, extra_property):
                self.property_ = property_
                self.other_property = other_property
                self.extra_property = extra_property

            all_properties = ['property_', 'other_property']

        test_object = TestObject(
            property_='value',
            other_property='otherValue',
            extra_property='extraValue',
        )

        assert_that(test_object.to_json_str(), is_(equal_to(
            '{"otherProperty": "otherValue", "property_": "value"}'
        )))

    def test_to_json_str_with_hidden_properties(self):
        @quickbean.AutoToJson
        class TestObject(object):
            def __init__(self, property_, other_property, _hidden_property):
                self.property_ = property_
                self.other_property = other_property
                self._hidden_property = _hidden_property

        test_object = TestObject(
            property_='value',
            other_property='otherValue',
            _hidden_property='hiddenValue',
        )

        assert_that(test_object.to_json_str(), is_(equal_to(
            '{"otherProperty": "otherValue", "property_": "value"}'
        )))

    def test_to_json_str_with_excluded_properties(self):
        @quickbean.AutoToJson(quickbean.exclude_properties('excluded_property'))
        class TestObject(object):
            def __init__(self, property_, other_property, excluded_property):
                self.property_ = property_
                self.other_property = other_property
                self.excluded_property = excluded_property

        test_object = TestObject(
            property_='value',
            other_property='otherValue',
            excluded_property='excludedValue',
        )

        assert_that(test_object.to_json_str(), is_(equal_to(
            '{"otherProperty": "otherValue", "property_": "value"}'
        )))

    def test_to_json_str_with_json_beans_as_properties_values(self):
        @quickbean.AutoToJson
        class TestObject(object):
            def __init__(self, property_, other_property):
                self.property_ = property_
                self.other_property = other_property

        test_object = TestObject(
            property_='value',
            other_property=TestObject(
                property_='value',
                other_property='otherValue',
            ),
        )

        assert_that(test_object.to_json_str(), is_(equal_to(
            '{"otherProperty": {"otherProperty": "otherValue", "property_": "value"}, "property_": "value"}'
        )))

    def test_to_json_str_with_lists_of_json_beans_as_properties_values(self):
        @quickbean.AutoToJson
        class TestObject(object):
            def __init__(self, property_, other_property):
                self.property_ = property_
                self.other_property = other_property

        test_object = TestObject(
            property_='value',
            other_property=[
                TestObject(
                    property_='value',
                    other_property='otherValue',
                ),
            ],
        )

        assert_that(test_object.to_json_str(), is_(equal_to(
            '{"otherProperty": [{"otherProperty": "otherValue", "property_": "value"}], "property_": "value"}'
        )))

    def test_to_json_str_with_dicts_of_json_beans_as_properties_values(self):
        @quickbean.AutoToJson
        class TestObject(object):
            def __init__(self, property_, other_property):
                self.property_ = property_
                self.other_property = other_property

        test_object = TestObject(
            property_='value',
            other_property={
                'key': TestObject(
                    property_='value',
                    other_property='otherValue',
                ),
            },
        )

        assert_that(test_object.to_json_str(), is_(equal_to(
            '{"otherProperty": {"key": {"otherProperty": "otherValue", "property_": "value"}}, "property_": "value"}'
        )))

    def test_to_json_str_with_properties_values_encoded_by_to_json_str_methods(self):
        @quickbean.AutoToJson
        class TestObject(object):
            def __init__(self, property_, other_property):
                self.property_ = property_
                self.other_property = other_property

            # noinspection PyMethodMayBeStatic
            def other_property_to_json_str(self):
                return json.dumps('%sToJson' % self.other_property)

        test_object = TestObject(
            property_='value',
            other_property='otherValue',
        )

        assert_that(test_object.to_json_str(), is_(equal_to(
            '{"otherProperty": "otherValueToJson", "property_": "value"}'
        )))

    def test_to_json_str_with_json_beans_as_properties_values_encoded_by_to_json_str_methods(self):
        @quickbean.AutoToJson
        class TestObject(object):
            def __init__(self, property_, other_property):
                self.property_ = property_
                self.other_property = other_property

            # noinspection PyMethodMayBeStatic
            def other_property_to_json_str(self):
                return json.dumps('%sToJson' % self.other_property.other_property)

        test_object = TestObject(
            property_='value',
            other_property=TestObject(
                property_='value',
                other_property='otherValue',
            ),
        )

        assert_that(test_object.to_json_str(), is_(equal_to(
            '{"otherProperty": "otherValueToJson", "property_": "value"}'
        )))

    def test_to_json_str_with_lists_of_json_beans_as_properties_values_encoded_by_to_json_str_methods(self):
        @quickbean.AutoToJson
        class TestObject(object):
            def __init__(self, property_, other_property):
                self.property_ = property_
                self.other_property = other_property

            # noinspection PyMethodMayBeStatic
            def other_property_to_json_str(self):
                return json.dumps('%sToJson' % self.other_property[0].other_property)

        test_object = TestObject(
            property_='value',
            other_property=[
                TestObject(
                    property_='value',
                    other_property='otherValue',
                ),
            ],
        )

        assert_that(test_object.to_json_str(), is_(equal_to(
            '{"otherProperty": "otherValueToJson", "property_": "value"}'
        )))

    def test_to_json_str_with_dicts_of_json_beans_as_properties_values_encoded_by_to_json_str_methods(self):
        @quickbean.AutoToJson
        class TestObject(object):
            def __init__(self, property_, other_property):
                self.property_ = property_
                self.other_property = other_property

            # noinspection PyMethodMayBeStatic
            def other_property_to_json_str(self):
                return json.dumps('%sToJson' % self.other_property['key'].other_property)

        test_object = TestObject(
            property_='value',
            other_property={
                'key': TestObject(
                    property_='value',
                    other_property='otherValue',
                ),
            },
        )

        assert_that(test_object.to_json_str(), is_(equal_to(
            '{"otherProperty": "otherValueToJson", "property_": "value"}'
        )))

    def test_to_json_str_with_properties_values_encoded_by_to_json_str_methods_of_custom_types(self):
        class CustomType(object):
            # noinspection PyMethodMayBeStatic
            def to_json_str(self, value):
                return json.dumps('%sToJson' % value)

        @quickbean.AutoToJson
        class TestObject(object):
            def __init__(self, property_, other_property):
                self.property_ = property_
                self.other_property = other_property

            other_property_type = CustomType()

        test_object = TestObject(
            property_='value',
            other_property='otherValue',
        )

        assert_that(test_object.to_json_str(), is_(equal_to(
            '{"otherProperty": "otherValueToJson", "property_": "value"}'
        )))

    def test_to_json_str_with_json_beans_as_properties_values_encoded_by_to_json_str_methods_of_custom_types(self):
        class CustomType(object):
            # noinspection PyMethodMayBeStatic
            def to_json_str(self, value):
                return json.dumps('%sToJson' % value.other_property)

        @quickbean.AutoToJson
        class TestObject(object):
            def __init__(self, property_, other_property):
                self.property_ = property_
                self.other_property = other_property

            other_property_type = CustomType()

        test_object = TestObject(
            property_='value',
            other_property=TestObject(
                property_='value',
                other_property='otherValue',
            ),
        )

        assert_that(test_object.to_json_str(), is_(equal_to(
            '{"otherProperty": "otherValueToJson", "property_": "value"}'
        )))

    # noinspection PyPep8
    def test_to_json_str_with_lists_of_json_beans_as_properties_values_encoded_by_to_json_str_methods_of_custom_types(self):
        class CustomType(object):
            # noinspection PyMethodMayBeStatic
            def to_json_str(self, value):
                return json.dumps('%sToJson' % value[0].other_property)

        @quickbean.AutoToJson
        class TestObject(object):
            def __init__(self, property_, other_property):
                self.property_ = property_
                self.other_property = other_property

            other_property_type = CustomType()

        test_object = TestObject(
            property_='value',
            other_property=[
                TestObject(
                    property_='value',
                    other_property='otherValue',
                ),
            ],
        )

        assert_that(test_object.to_json_str(), is_(equal_to(
            '{"otherProperty": "otherValueToJson", "property_": "value"}'
        )))

    # noinspection PyPep8
    def test_to_json_str_with_dicts_of_json_beans_as_properties_values_encoded_by_to_json_str_methods_of_custom_types(self):
        class CustomType(object):
            # noinspection PyMethodMayBeStatic
            def to_json_str(self, value):
                return json.dumps('%sToJson' % value['key'].other_property)

        @quickbean.AutoToJson
        class TestObject(object):
            def __init__(self, property_, other_property):
                self.property_ = property_
                self.other_property = other_property

            other_property_type = CustomType()

        test_object = TestObject(
            property_='value',
            other_property={
                'key': TestObject(
                    property_='value',
                    other_property='otherValue',
                ),
            },
        )

        assert_that(test_object.to_json_str(), is_(equal_to(
            '{"otherProperty": "otherValueToJson", "property_": "value"}'
        )))

    def test_to_json_str_with_descriptors(self):
        @quickbean.AutoToJson
        class TestObject(object):
            def __init__(self, property_, other_property):
                self._properties = {
                    'property_': property_,
                    'other_property': other_property,
                }

            @property
            def property_(self):
                return self._properties['property_']

            @property
            def other_property(self):
                return self._properties['other_property']

        test_object = TestObject(
            property_='value',
            other_property='otherValue',
        )

        assert_that(test_object.to_json_str(), is_(equal_to(
            '{"otherProperty": "otherValue", "property_": "value"}'
        )))

    def test_to_pretty_json_str(self):
        @quickbean.AutoToJson
        class TestObject(object):
            def __init__(self, property_, other_property):
                self.property_ = property_
                self.other_property = other_property

        test_object = TestObject(
            property_='value',
            other_property='otherValue',
        )

        assert_that(test_object.to_pretty_json_str(), is_(equal_to(textwrap.dedent(
            """\
            {
                "otherProperty": "otherValue",
                "property_": "value"
            }"""
        ))))

    def test_list_to_json_dict(self):
        @quickbean.AutoToJson
        class TestObject(object):
            def __init__(self, property_):
                self.property_ = property_

        test_object = TestObject(
            property_='value',
        )
        other_test_object = TestObject(
            property_='otherValue',
        )

        assert_that(TestObject.list_to_json_dict([test_object, other_test_object]), is_(equal_to(
            [{'property_': 'value'}, {'property_': 'otherValue'}]
        )))

    def test_list_to_json_str(self):
        @quickbean.AutoToJson
        class TestObject(object):
            def __init__(self, property_):
                self.property_ = property_

        test_object = TestObject(
            property_='value',
        )
        other_test_object = TestObject(
            property_='otherValue',
        )

        assert_that(TestObject.list_to_json_str([test_object, other_test_object]), is_(equal_to(
            '[{"property_": "value"}, {"property_": "otherValue"}]'
        )))


# noinspection PyMethodMayBeStatic
class TestExcludeHiddenProperties(object):
    def test_on_hidden_property(self):
        properties_filter = quickbean.exclude_hidden_properties()

        assert_that(properties_filter('_hidden'), is_(False))

    def test_on_non_hidden_property(self):
        properties_filter = quickbean.exclude_hidden_properties()

        assert_that(properties_filter('non_hidden'), is_(True))


# noinspection PyMethodMayBeStatic
class TestExcludeProperties(object):
    def test_on_excluded_property(self):
        properties_filter = quickbean.exclude_properties('excluded_property', 'other_excluded_property')

        assert_that(properties_filter('excluded_property'), is_(False))

    def test_on_non_excluded_property(self):
        properties_filter = quickbean.exclude_properties('excluded_property', 'other_excluded_property')

        assert_that(properties_filter('non_excluded_property'), is_(True))


# noinspection PyMethodMayBeStatic
class TestOnlyIncludeProperties(object):
    def test_on_included_property(self):
        properties_filter = quickbean.only_include_properties('included_property', 'other_included_property')

        assert_that(properties_filter('included_property'), is_(True))

    def test_on_non_included_property(self):
        properties_filter = quickbean.only_include_properties('included_property', 'other_included_property')

        assert_that(properties_filter('non_included_property'), is_(False))
